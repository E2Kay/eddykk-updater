package application;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javafx.application.Platform;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

import org.apache.commons.io.FileUtils;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

public class Installer extends Thread {
	private String client;
	private Label wStatusLabel;
	private Label pStatusLabel;
	private Label gStatusLabel;
	private CheckBox wDeleteBox;
	
	Installer(String clientName, Label doneLabel, CheckBox dbox) {
		client = clientName;
		wStatusLabel = doneLabel;
		pStatusLabel = doneLabel;
		gStatusLabel = doneLabel;
		wDeleteBox = dbox;
	}

	@Override
	public void run() {
		// WoT
		if (client.equals("WoT")) {
			// Downloads zip-file.
			Platform.runLater(new Runnable() { 

				public void run() { 
					wStatusLabel.setText("Downloading...");
				}
			});
					
			try {
				URL url = new URL(Updater.wUrl);
				File destination = new File(Updater.tmpd + "/" + Updater.wName);
				FileUtils.copyURLToFile(url, destination);
			} 
			catch (IOException e) {
				e.printStackTrace();
				Platform.runLater(new Runnable() {

					public void run() {
						wStatusLabel.setText("Error: Couldn't download zip.");
					}
				});
				return;
			}
					
			// Extracts the zip-file.
			Platform.runLater(new Runnable() { 

				public void run() { 
					wStatusLabel.setText("Extracting...");
				}
			});
					
			try {
				ZipFile zipFile = new ZipFile(Updater.tmpd + "/" + Updater.wName);
				zipFile.extractAll(Updater.tmpd + "/" + Updater.wName.substring(0, Updater.wName.length() -4) );
			} catch (ZipException e) {
				e.printStackTrace();
				Platform.runLater(new Runnable() { 

					public void run() { 
						wStatusLabel.setText("Error: Couldn't extract zip.");
					}
				});
				return;
			}
			
			// Delete current mods.
			if(wDeleteBox.isSelected()) {
				Platform.runLater(new Runnable() { 

					public void run() { 
						wStatusLabel.setText("Deleting current mods...");
					}
				});
				
				try {
					File target = new File (FolderChooser.wDir + "/res_mods/");
					FileUtils.cleanDirectory(target);
				}catch(Exception e){
					e.printStackTrace();
					Platform.runLater(new Runnable() { 

						public void run() { 
							wStatusLabel.setText("Error: Couldn't delete current mods.");
						}
					});
					return;
				}
			}
			
			// Copies extracted folder to a destination directory.
			Platform.runLater(new Runnable() { 

				public void run() { 
					wStatusLabel.setText("Copying...");
				}
			});
					
			try {			
				String source = Updater.tmpd + "/" + Updater.wName.substring(0, Updater.wName.length() -4);
				File sourceDir = new File(source);
				String destination = FolderChooser.wDir.toString();
				File targetDir = new File(destination);
				FileUtils.copyDirectory(sourceDir, targetDir);
			} catch (IOException e) {
				e.printStackTrace();
				Platform.runLater(new Runnable() { 

					public void run() { 
						wStatusLabel.setText("Error: Couldn't copy content.");
					}
				});
				return;
			}
					
			Platform.runLater(new Runnable() { 

				public void run() { 
					wStatusLabel.setText("Modpack installation is done!");
				}
			});
		}
				
				
		// Pidgin
		if (client.equals("Pidgin")) {
			// Downloads zip-file.
			Platform.runLater(new Runnable() { 

				public void run() { 
					pStatusLabel.setText("Downloading...");
				}
			});
					
			try {
				URL url = new URL(Updater.pUrl);
			    File destination = new File(Updater.tmpd + "/" + Updater.pName);
			    FileUtils.copyURLToFile(url, destination);
			} 
			catch (IOException e) {
				e.printStackTrace();
				Platform.runLater(new Runnable() { 

					public void run() { 
						pStatusLabel.setText("Error: Couldn't download zip.");
					}
				});
				return;
			}
					
			// Extracts the zip-file.
			Platform.runLater(new Runnable() { 

				public void run() { 
					pStatusLabel.setText("Extracting...");
				}
			});
					
			try {
				ZipFile zipFile = new ZipFile(Updater.tmpd + "/" + Updater.pName);
				zipFile.extractAll(Updater.tmpd + "/" + Updater.pName.substring(0, Updater.pName.length() -4) );
			} catch (ZipException e) {
				e.printStackTrace();
				Platform.runLater(new Runnable() { 

					public void run() { 
						pStatusLabel.setText("Error: Couldn't extract zip.");
					}
				});
				return;
			}
					
			// Copies extracted folder to a destination directory.
			Platform.runLater(new Runnable() { 

				public void run() { 
					pStatusLabel.setText("Copying...");
				}
			});
					
			try {			
				String source = Updater.tmpd + "/" + Updater.pName.substring(0, Updater.pName.length() -4);
				File sourceDir = new File(source);
				String destination = FolderChooser.pDir.toString();
				File targetDir = new File(destination + "/pixmaps/pidgin/emotes/");
				FileUtils.copyDirectory(sourceDir, targetDir);
			} catch (IOException e) {
				e.printStackTrace();
				Platform.runLater(new Runnable() { 

					public void run() { 
						pStatusLabel.setText("Error: Couldn't copy content.");
					}
				});
				return;
			}
					
			Platform.runLater(new Runnable() { 

				public void run() { 
					pStatusLabel.setText("Smile Pack installation is done!");
				}
			});
		}
					
				
		// Gajim
		if (client.equals("Gajim")) {
			// Downloads zip-file.
			Platform.runLater(new Runnable() { 

				public void run() { 
					gStatusLabel.setText("Downloading...");
				}
			});
					
			try {
				URL url = new URL(Updater.gUrl);
			    File destination = new File(Updater.tmpd + "/" + Updater.gName);
			    FileUtils.copyURLToFile(url, destination);
			} 
			catch (IOException e) {
				e.printStackTrace();
				Platform.runLater(new Runnable() { 

					public void run() { 
						gStatusLabel.setText("Error: Couldn't download zip.");
					}
				});
				return;
			}
					
			// Extracts the zip-file.
					
			Platform.runLater(new Runnable() { 

				public void run() { 
					gStatusLabel.setText("Extracting...");
				}
			});
					
			try {
				ZipFile zipFile = new ZipFile(Updater.tmpd + "/" + Updater.gName);
				zipFile.extractAll(Updater.tmpd + "/" + Updater.gName.substring(0, Updater.gName.length() -4) );
			} catch (ZipException e) {
				e.printStackTrace();
				Platform.runLater(new Runnable() { 

					public void run() { 
						gStatusLabel.setText("Error: Couldn't exctract zip.");
					}
				});
				return;
			}
					
			// Copies extracted folder to a destination directory.
			Platform.runLater(new Runnable() { 

				public void run() { 
					gStatusLabel.setText("Copying...");
				}
			});
					
			try {			
				String source = Updater.tmpd + "/" + Updater.gName.substring(0, Updater.gName.length() -4);
				File sourceDir = new File(source);
				String destination = FolderChooser.gDir.toString();
				File targetDir = new File(destination + "/data/emoticons");
				FileUtils.copyDirectory(sourceDir, targetDir);
			} catch (IOException e) {
				e.printStackTrace();
				Platform.runLater(new Runnable() { 

					public void run() { 
						gStatusLabel.setText("Error: Couldn't copy content.");
					}
				});
				return;
			}
					
			Platform.runLater(new Runnable() { 

				public void run() { 
					gStatusLabel.setText("Smile Pack installation is done!");
				}
			});
		}
	}
}
