package application;

import java.io.IOException;

import javafx.application.Platform;
import javafx.scene.control.Label;

import org.json.JSONException;

public class Updater extends Thread{
	static String wUrl = null;
	static String pUrl = null;
	static String gUrl = null;
	static String wName = null;
	static String pName = null;
	static String gName = null;
	static String tmpd = System.getProperty("java.io.tmpdir");
	private Label wVersionLabel;
	private Label pVersionLabel;
	private Label gVersionLabel;
	
	Updater(Label wLabel, Label pLabel, Label gLabel) {
		wVersionLabel = wLabel;
		pVersionLabel = pLabel;
		gVersionLabel = gLabel;
	}
	
	public void run() {
		Parser parsi = new Parser();
		try {
			wName = parsi.Tiedostonimi("wotmodpack", "").getString("content");
			pName = parsi.Tiedostonimi("smile", "pidgin").getString("content");
			gName = parsi.Tiedostonimi("smile", "gajim").getString("content");
			wUrl = "https://eddykk.com/wot/" + wName;
			pUrl = "https://eddykk.com/smiley/pidgin/" + pName;
			gUrl = "https://eddykk.com/smiley/gajim/" + gName;
			Platform.runLater(new Runnable() { 

				public void run() {
					wVersionLabel.setText(wName.substring(18, wName.length() -4) );
					pVersionLabel.setText(pName.substring(6, pName.length() -11) );
					gVersionLabel.setText(gName.substring(6, gName.length() -10) );
				}
			});
		}
		catch (JSONException e) {
			System.out.println(e.toString());
		}
		catch (IOException e) {
			System.out.println(e.toString());
		}
	}
}