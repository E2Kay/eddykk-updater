package application;

import java.io.File;

import javafx.stage.DirectoryChooser;
import javafx.stage.Window;


public class FolderChooser {
	public static File wDir;
	public static File pDir;
	public static File gDir;
	
	public static void chooseFolder(String title, String clientlocation) {
		DirectoryChooser chooser = new DirectoryChooser();
			chooser.setTitle(title);
			File defaultDirectory = new File(clientlocation);
			chooser.setInitialDirectory(defaultDirectory);
			Window primaryStage = null;
			File selectedDirectory = chooser.showDialog(primaryStage);
			if (title.equals("Set WoT directory")) {
				wDir = selectedDirectory;
			}
			if (title.equals("Set Pidgin directory")) {
				pDir = selectedDirectory;
			}
			if (title.equals("Set Gajim directory")) {
				gDir = selectedDirectory;
			}
	}
}
