package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONException;
import org.json.JSONObject;

public class Parser {

  private static String readAll(Reader rd) throws IOException {
    StringBuilder sb = new StringBuilder();
    int cp;
    while ((cp = rd.read()) != -1) {
      sb.append((char) cp);
    }
    return sb.toString();
  }

  public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
    InputStream is = new URL(url).openStream();
    try {
      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
      String jsonText = readAll(rd);
      JSONObject json = new JSONObject(jsonText);
      return json;
    } finally {
      is.close();
    }
  }

  public JSONObject Tiedostonimi(String service, String type) throws IOException, JSONException {
	String url = "https://eddykk.com/api/latest.php?";
	if (service.equals("wotmodpack")) {
		url += "service=wot";
	}
	else if (service.equals("smile")) {
		  if (type.equals("gajim")) {
			  url += "service=smile&type=gajim";
		  }
		  else if (type.equals("pidgin")) {
			  url += "service=smile&type=pidgin";		  
		  }
	}
	JSONObject json = readJsonFromUrl(url);
	return json;
  }
}