package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

public class Controller implements Initializable {
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		if(FolderChooser.wDir == null){
			wLocationLabel.setText("Directory is not selected");
		}
		if(FolderChooser.pDir == null){
			pLocationLabel.setText("Directory is not selected");
		}
		if(FolderChooser.gDir == null){
			gLocationLabel.setText("Directory is not selected");
		}
		Updater update = new Updater (wVersionLabel, pVersionLabel, gVersionLabel);
		update.start();
	}
	
	@FXML
	Label wVersionLabel = new Label();
	@FXML
	Label wLocationLabel = new Label();
	@FXML
	Label wConditionLabel = new Label();
	@FXML 
	Label pVersionLabel = new Label();
	@FXML
	Label pLocationLabel = new Label();
	@FXML
    Label pConditionLabel = new Label();
	@FXML 
	Label gVersionLabel = new Label();
	@FXML
	Label gLocationLabel = new Label();
    @FXML
    Label gConditionLabel = new Label();
    @FXML
    CheckBox wDeleteBox = new CheckBox();
	
	// Wot
	public void updateWot(ActionEvent event) {
		Updater update = new Updater (wVersionLabel, pVersionLabel, gVersionLabel);
		update.start();
	}
	
	public void chooseWotLocation(ActionEvent event) {
		FolderChooser.chooseFolder("Set WoT directory", "/");
		wLocationLabel.setText(FolderChooser.wDir.toString());
	}
	
	public void checkDelete(ActionEvent event) {
		boolean selected = wDeleteBox.isSelected();
	}
	
	public void installWot(ActionEvent event) {
		Installer wot = new Installer("WoT", wConditionLabel, wDeleteBox);
		wot.start();
	}
	
	// Pidgin
	public void updatePidgin(ActionEvent event) {
		Updater update = new Updater (wVersionLabel, pVersionLabel, gVersionLabel);
		update.start();
	}
	
	public void choosePidginLocation(ActionEvent event) {
		FolderChooser.chooseFolder("Set Pidgin directory", "/");
		pLocationLabel.setText(FolderChooser.pDir.toString());
	}
	
	public void installPidgin(ActionEvent event) {
		Installer pidgin = new Installer("Pidgin", pConditionLabel, wDeleteBox);
		pidgin.start();
	}
	
	// Gajim
	public void updateGajim(ActionEvent event) {
		Updater update = new Updater (wVersionLabel, pVersionLabel, gVersionLabel);
		update.start();
	}
	
	public void chooseGajimLocation(ActionEvent event) {
		FolderChooser.chooseFolder("Set Gajim directory", "/");
		gLocationLabel.setText(FolderChooser.gDir.toString());
	}
	
	public void installGajim(ActionEvent event) {
		Installer gajim = new Installer("Gajim", gConditionLabel, wDeleteBox);
		gajim.start();
	}
}
